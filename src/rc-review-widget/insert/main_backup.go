package main

// import (
// 	"encoding/json"
// 	"errors"
// 	"fmt"
// 	"net/http"
// 	"os"

// 	"github.com/aws/aws-lambda-go/events"
// 	"github.com/aws/aws-lambda-go/lambda"
// 	"github.com/aws/aws-sdk-go/aws"
// 	"github.com/aws/aws-sdk-go/aws/session"
// 	"github.com/aws/aws-sdk-go/service/dynamodb"
// 	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
// )

// type ReviewWidget struct {
// 	HotelID     int64    `json:"hotelId"`
// 	HotelName   string   `json:"hotelName"`
// 	ReviewCount int64    `json:"reviewCount"`
// 	Reputation  string   `json:"reputation"`
// 	GoodRate    string   `json:"goodRate"`
// 	Reviews     []Review `json:"reviews"`
// 	Config      Config   `json:"config"`
// 	CreatedAt   string   `json:"createdAt"`
// 	UpdatedAt   string   `json:"updatedAt"`
// }

// type Review struct {
// 	Comment            string `json:"comment"`
// 	GetUrl             string `json:"getUrl"`
// 	PostDate           string `json:"postDate"`
// 	TotalScore         string `json:"totalScore"`
// 	TotalOriginalScore string `json:"totalOriginalScore"`
// 	SiteID             string `json:"siteID"`
// 	SiteColor          string `json:"siteColor"`
// 	SiteName           string `json:"siteName"`
// }

// type Config struct {
// 	OnlySelectedReviews bool     `json:"onlySelectedReviews"`
// 	OnlySelectedOtas    bool     `json:"onlySelectedOtas"`
// 	OtaNames            []string `json:"otaNames"`
// }

// type ErrorBody struct {
// 	ErrorMsg *string `json:"error,omitempty"`
// }

// func ApiResponse(status int, body interface{}) (events.APIGatewayProxyResponse, error) {
// 	resp := events.APIGatewayProxyResponse{}
// 	resp.StatusCode = status
// 	resp.Headers = map[string]string{
// 		"Access-Control-Allow-Origin":  "*",
// 		"Access-Control-Allow-Headers": "*",
// 		// "Access-Control-Allow-Headers": "origin,Accept,Authorization,Content-Type,X-Amz-Date,X-Api-Key,X-Amz-Security-Token",
// 		"Access-Control-Allow-Methods": "DELETE,GET,OPTIONS,POST,PUT",
// 		"Content-Type":                 "application/json",
// 	}
// 	stringBody, _ := json.Marshal(body)
// 	resp.Body = string(stringBody)
// 	return resp, nil
// }

// func Dynamodb() *dynamodb.DynamoDB {
// 	// Environment variables
// 	endpoint := os.Getenv("DYNAMODB_ENDPOINT")
// 	region := os.Getenv("AWS_REGION")
// 	// DynamoDB
// 	sess := session.Must(session.NewSession())
// 	config := aws.NewConfig().WithRegion(region)
// 	if len(endpoint) > 0 {
// 		config = config.WithEndpoint(endpoint)
// 	}
// 	return dynamodb.New(sess, config)
// }

// func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

// 	// Table name
// 	tableName := "ReviewWidget"
// 	// Request path parameter
// 	hotel_id, ok := request.PathParameters["hotel_id"]
// 	if ok == false {
// 		return ApiResponse(http.StatusBadRequest, ErrorBody{
// 			ErrorMsg: aws.String(errors.New("HotelIdNotFound error").Error()),
// 		})
// 	}
// 	// DynamoDB
// 	db := Dynamodb()
// 	// input for GetItem
// 	input := &dynamodb.QueryInput{
// 		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
// 			":hotel_id": {
// 				N: aws.String(hotel_id),
// 			},
// 		},
// 		KeyConditionExpression: aws.String("hotelId = :hotel_id"),
// 		TableName:              aws.String(tableName),
// 	}
// 	// GetItem from dynamodb table
// 	result, err := db.Query(input)
// 	if err != nil {
// 		fmt.Println("err :", err)
// 		fmt.Println("input :", input)
// 		return ApiResponse(http.StatusBadRequest, ErrorBody{
// 			ErrorMsg: aws.String(errors.New("FetchRecord error").Error()),
// 		})
// 	}

// 	item1 := new([]*ReviewWidget)
// 	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &item1)
// 	if err != nil {
// 		fmt.Println("err 3:", err)
// 		return ApiResponse(http.StatusBadRequest, ErrorBody{
// 			ErrorMsg: aws.String(errors.New("unmarshal error").Error()),
// 		})
// 	}
// 	return ApiResponse(http.StatusOK, item1)
// }
// func main() {
// 	lambda.Start(handler)
// }
