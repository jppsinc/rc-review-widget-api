package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

type ReviewWidget struct {
	HotelID     int64    `json:"hotelId"`
	HotelName   string   `json:"hotelName"`
	ReviewCount int64    `json:"reviewCount"`
	Reputation  string   `json:"reputation"`
	GoodRate    string   `json:"goodRate"`
	Reviews     []Review `json:"reviews"`
	Config      Config   `json:"config"`
	CreatedAt   string   `json:"createdAt"`
	UpdatedAt   string   `json:"updatedAt"`
}

type Review struct {
	Comment            string `json:"comment"`
	GetUrl             string `json:"getUrl"`
	PostDate           string `json:"postDate"`
	TotalScore         string `json:"totalScore"`
	TotalOriginalScore string `json:"totalOriginalScore"`
	SiteID             string `json:"siteID"`
	SiteColor          string `json:"siteColor"`
	SiteName           string `json:"siteName"`
}

type Config struct {
	OnlySelectedReviews bool     `json:"onlySelectedReviews"`
	OnlySelectedOtas    bool     `json:"onlySelectedOtas"`
	OtaNames            []string `json:"otaNames"`
}

type ErrorBody struct {
	ErrorMsg *string `json:"error,omitempty"`
}

func ApiResponse(status int, body interface{}) (events.APIGatewayProxyResponse, error) {
	resp := events.APIGatewayProxyResponse{}
	resp.StatusCode = status
	resp.Headers = map[string]string{
		"Access-Control-Allow-Origin":  "*",
		"Access-Control-Allow-Headers": "*",
		// "Access-Control-Allow-Headers": "origin,Accept,Authorization,Content-Type,X-Amz-Date,X-Api-Key,X-Amz-Security-Token",
		"Access-Control-Allow-Methods": "DELETE,GET,OPTIONS,POST,PUT",
		"Content-Type":                 "application/json",
	}
	stringBody, _ := json.Marshal(body)
	resp.Body = string(stringBody)
	return resp, nil
}

func Dynamodb() *dynamodb.DynamoDB {
	// Environment variables
	endpoint := os.Getenv("DYNAMODB_ENDPOINT")
	region := os.Getenv("AWS_REGION")
	// DynamoDB
	sess := session.Must(session.NewSession())
	config := aws.NewConfig().WithRegion(region)
	if len(endpoint) > 0 {
		config = config.WithEndpoint(endpoint)
	}
	return dynamodb.New(sess, config)
}

func DateTimeStamp() string {
	//set timezone
	jst, _ := time.LoadLocation("Asia/Tokyo")
	s := time.Now().In(jst).Format("2006/01/02 15:04:05")
	return s
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	// Table name
	tableName := "ReviewWidget"

	var sds ReviewWidget

	if err := json.Unmarshal([]byte(request.Body), &sds); err != nil {
		fmt.Println("Unmarshal :", err)
		return ApiResponse(http.StatusBadRequest, ErrorBody{
			ErrorMsg: aws.String(errors.New("ErrorCouldNotUnMarshalItem error").Error()),
		})
	}

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]

	if ok == false {
		return ApiResponse(http.StatusBadRequest, ErrorBody{
			ErrorMsg: aws.String(errors.New("HotelIdNotFound error").Error()),
		})
	} else {
		i, err := strconv.Atoi(hotel_id)
		if err != nil {
			fmt.Println(err)
		}
		sds.HotelID = int64(i)
		sds.CreatedAt = DateTimeStamp()
		sds.UpdatedAt = DateTimeStamp()
	}

	if len(sds.Config.OtaNames) == 0 {
		sds.Config.OtaNames = []string{}
	}

	// // DynamoDB
	db := Dynamodb()

	// change struct data to json data, this data will be stored to database
	av, err := dynamodbattribute.MarshalMap(sds)
	if err != nil {
		return ApiResponse(http.StatusBadRequest, ErrorBody{
			ErrorMsg: aws.String(errors.New("CouldNotMarshalItem error").Error()),
		})
	}

	if len(sds.Reviews) == 0 {
		empty := []*dynamodb.AttributeValue{}
		av["reviews"] = &dynamodb.AttributeValue{L: empty}
	}

	ReturnItem := "SIZE"
	input := &dynamodb.PutItemInput{
		Item:                        av,
		TableName:                   aws.String(tableName),
		ReturnItemCollectionMetrics: &ReturnItem,
	}

	_, err = db.PutItem(input)
	if err != nil {
		fmt.Println(err)
		return ApiResponse(http.StatusBadRequest, ErrorBody{
			ErrorMsg: aws.String(errors.New("CouldNotDynamoPutItem Error").Error()),
		})
	}
	return ApiResponse(http.StatusCreated, &sds)
}
func main() {
	lambda.Start(handler)
}
