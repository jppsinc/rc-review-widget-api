package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

type ReviewWidget struct {
	HotelID   int    `json:"hotelId"`
	Config    Config `json:"config"`
	UpdatedAt string `json:"updatedAt"`
}
type Config struct {
	OnlySelectedReviews bool     `json:"onlySelectedReviews"`
	OnlySelectedOtas    bool     `json:"onlySelectedOtas"`
	OtaNames            []string `json:"otaNames"`
}
type ErrorBody struct {
	ErrorMsg *string `json:"error,omitempty"`
}

func ApiResponse(status int, body interface{}) (events.APIGatewayProxyResponse, error) {
	resp := events.APIGatewayProxyResponse{}
	resp.StatusCode = status
	resp.Headers = map[string]string{
		"Access-Control-Allow-Origin":  "*",
		"Access-Control-Allow-Headers": "*",
		// "Access-Control-Allow-Headers": "origin,Accept,Authorization,Content-Type,X-Amz-Date,X-Api-Key,X-Amz-Security-Token",
		"Access-Control-Allow-Methods": "DELETE,GET,OPTIONS,POST,PUT",
		"Content-Type":                 "application/json",
	}
	stringBody, _ := json.Marshal(body)
	resp.Body = string(stringBody)
	return resp, nil
}
func Dynamodb() *dynamodb.DynamoDB {
	// Environment variables
	endpoint := os.Getenv("DYNAMODB_ENDPOINT")
	region := os.Getenv("AWS_REGION")
	// DynamoDB
	sess := session.Must(session.NewSession())
	config := aws.NewConfig().WithRegion(region)
	if len(endpoint) > 0 {
		config = config.WithEndpoint(endpoint)
	}
	return dynamodb.New(sess, config)
}
func DateTimeStamp() string {
	//set timezone
	jst, _ := time.LoadLocation("Asia/Tokyo")
	s := time.Now().In(jst).Format("2006/01/02 15:04:05")
	return s
}
func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	// Table name
	tableName := "ReviewWidget"
	var sds ReviewWidget
	if err := json.Unmarshal([]byte(request.Body), &sds); err != nil {
		fmt.Println("Unmarshal :", err)
		return ApiResponse(http.StatusBadRequest, ErrorBody{
			ErrorMsg: aws.String(errors.New("ErrorCouldNotUnMarshalItem error").Error()),
		})
	}
	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false {
		return ApiResponse(http.StatusBadRequest, ErrorBody{
			ErrorMsg: aws.String(errors.New("HotelIdNotFound error").Error()),
		})
	} else {
		i, err := strconv.Atoi(hotel_id)
		if err != nil {
			fmt.Println(err)
		}
		sds.HotelID = int(i)
		sds.UpdatedAt = DateTimeStamp()
	}
	// // DynamoDB
	db := Dynamodb()

	if len(sds.Config.OtaNames) == 0 {
		sds.Config.OtaNames = []string{}
	}

	/*Task 2: update booking in booking table*/
	update := expression.
		Set(expression.Name("config"), expression.Value(sds.Config)).
		Set(expression.Name("updatedAt"), expression.Value(sds.UpdatedAt))
	expr, err := expression.NewBuilder().
		WithUpdate(update).
		Build()
	fmt.Println("TransactWriteItems 1")
	// Transaction start: teansaction api is atomic in nature
	_, err = db.TransactWriteItems(&dynamodb.TransactWriteItemsInput{
		TransactItems: []*dynamodb.TransactWriteItem{
			// GroupHotel
			{
				// conditional put, check if facility code is present/same then only insert
				Update: &dynamodb.Update{
					TableName: aws.String(tableName),
					Key: map[string]*dynamodb.AttributeValue{
						"hotelId": {
							N: aws.String(hotel_id),
						},
					},
					ExpressionAttributeNames:  expr.Names(),
					ExpressionAttributeValues: expr.Values(),
					UpdateExpression:          expr.Update(),
				},
			},
		},
	})
	if err != nil {
		fmt.Println(err)
		return ApiResponse(http.StatusBadRequest, ErrorBody{
			ErrorMsg: aws.String(errors.New("TransactWriteItems Error").Error()),
		})
	}
	return ApiResponse(http.StatusOK, sds)
}
func main() {
	lambda.Start(handler)
}
