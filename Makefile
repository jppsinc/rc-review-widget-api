
.PHONY: all

all: build

build:
# rc-review-widget
	GOOS=linux GOARCH=amd64 $(MAKE) rw_index
	GOOS=linux GOARCH=amd64 $(MAKE) rw_update
	GOOS=linux GOARCH=amd64 $(MAKE) rw_insert 

# review-widget
rw_index: ./src/rc-review-widget/index/main.go
	go build -o ./src/rc-review-widget/index/index ./src/rc-review-widget/index

rw_update: ./src/rc-review-widget/update/main.go
	go build -o ./src/rc-review-widget/update/update ./src/rc-review-widget/update

rw_insert: ./src/rc-review-widget/insert/main.go
	go build -o ./src/rc-review-widget/insert/insert ./src/rc-review-widget/insert


.PHONY: clean
clean: 

# review-widget
	rm -f ./src/rc-review-widget/review/review

